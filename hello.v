%entry
12 4
28 20
44 36
60 52
76 68
92 84
108 100
124 116
%entry
12 4
28 20
44 36
60 52
76 68
92 84
108 100
124 116
140 132
156 148
172 164
188 180
204 196
220 212
236 228
252 244
%entry
36 40
56 60

  .text
  .globl sum
sum:
  addi sp, sp, -240
  sw ra, 0(sp)
  li t0, 8
  add t0, t0, sp
  sw t0, 4(sp)
  sw a0, 12(sp)
  lw t1, 4(sp)
  lw t0, 12(sp)
  sw t0, 0(t1)
  li t0, 24
  add t0, t0, sp
  sw t0, 20(sp)
  sw a1, 28(sp)
  lw t1, 20(sp)
  lw t0, 28(sp)
  sw t0, 0(t1)
  li t0, 40
  add t0, t0, sp
  sw t0, 36(sp)
  sw a2, 44(sp)
  lw t1, 36(sp)
  lw t0, 44(sp)
  sw t0, 0(t1)
  li t0, 56
  add t0, t0, sp
  sw t0, 52(sp)
  sw a3, 60(sp)
  lw t1, 52(sp)
  lw t0, 60(sp)
  sw t0, 0(t1)
  li t0, 72
  add t0, t0, sp
  sw t0, 68(sp)
  sw a4, 76(sp)
  lw t1, 68(sp)
  lw t0, 76(sp)
  sw t0, 0(t1)
  li t0, 88
  add t0, t0, sp
  sw t0, 84(sp)
  sw a5, 92(sp)
  lw t1, 84(sp)
  lw t0, 92(sp)
  sw t0, 0(t1)
  li t0, 104
  add t0, t0, sp
  sw t0, 100(sp)
  sw a6, 108(sp)
  lw t1, 100(sp)
  lw t0, 108(sp)
  sw t0, 0(t1)
  li t0, 120
  add t0, t0, sp
  sw t0, 116(sp)
  sw a7, 124(sp)
  lw t1, 116(sp)
  lw t0, 124(sp)
  sw t0, 0(t1)
  lw t0, 4(sp)
  lw t0, 0(t0)
  sw t0, 132(sp)
  lw t0, 20(sp)
  lw t0, 0(t0)
  sw t0, 136(sp)
  lw t0, 132(sp)
  lw t1, 136(sp)
  add t0, t0, t1
  sw t0, 140(sp)
  lw t0, 36(sp)
  lw t0, 0(t0)
  sw t0, 148(sp)
  lw t0, 140(sp)
  lw t1, 148(sp)
  add t0, t0, t1
  sw t0, 152(sp)
  lw t0, 52(sp)
  lw t0, 0(t0)
  sw t0, 160(sp)
  lw t0, 152(sp)
  lw t1, 160(sp)
  add t0, t0, t1
  sw t0, 164(sp)
  lw t0, 68(sp)
  lw t0, 0(t0)
  sw t0, 172(sp)
  lw t0, 164(sp)
  lw t1, 172(sp)
  add t0, t0, t1
  sw t0, 176(sp)
  lw t0, 84(sp)
  lw t0, 0(t0)
  sw t0, 184(sp)
  lw t0, 176(sp)
  lw t1, 184(sp)
  add t0, t0, t1
  sw t0, 188(sp)
  lw t0, 100(sp)
  lw t0, 0(t0)
  sw t0, 196(sp)
  lw t0, 188(sp)
  lw t1, 196(sp)
  add t0, t0, t1
  sw t0, 200(sp)
  lw t0, 116(sp)
  lw t0, 0(t0)
  sw t0, 208(sp)
  lw t0, 200(sp)
  lw t1, 208(sp)
  add t0, t0, t1
  sw t0, 212(sp)
  lw a0, 212(sp)
  lw ra, 0(sp)
  addi sp, sp, 240
  ret

  .text
  .globl sum2
sum2:
  addi sp, sp, -464
  sw ra, 0(sp)
  li t0, 8
  add t0, t0, sp
  sw t0, 4(sp)
  sw a0, 12(sp)
  lw t1, 4(sp)
  lw t0, 12(sp)
  sw t0, 0(t1)
  li t0, 24
  add t0, t0, sp
  sw t0, 20(sp)
  sw a1, 28(sp)
  lw t1, 20(sp)
  lw t0, 28(sp)
  sw t0, 0(t1)
  li t0, 40
  add t0, t0, sp
  sw t0, 36(sp)
  sw a2, 44(sp)
  lw t1, 36(sp)
  lw t0, 44(sp)
  sw t0, 0(t1)
  li t0, 56
  add t0, t0, sp
  sw t0, 52(sp)
  sw a3, 60(sp)
  lw t1, 52(sp)
  lw t0, 60(sp)
  sw t0, 0(t1)
  li t0, 72
  add t0, t0, sp
  sw t0, 68(sp)
  sw a4, 76(sp)
  lw t1, 68(sp)
  lw t0, 76(sp)
  sw t0, 0(t1)
  li t0, 88
  add t0, t0, sp
  sw t0, 84(sp)
  sw a5, 92(sp)
  lw t1, 84(sp)
  lw t0, 92(sp)
  sw t0, 0(t1)
  li t0, 104
  add t0, t0, sp
  sw t0, 100(sp)
  sw a6, 108(sp)
  lw t1, 100(sp)
  lw t0, 108(sp)
  sw t0, 0(t1)
  li t0, 120
  add t0, t0, sp
  sw t0, 116(sp)
  sw a7, 124(sp)
  lw t1, 116(sp)
  lw t0, 124(sp)
  sw t0, 0(t1)
  li t0, 136
  add t0, t0, sp
  sw t0, 132(sp)
  lw t0, 468(sp)
  sw t0, 140(sp)
  lw t1, 132(sp)
  lw t0, 140(sp)
  sw t0, 0(t1)
  li t0, 152
  add t0, t0, sp
  sw t0, 148(sp)
  lw t0, 472(sp)
  sw t0, 156(sp)
  lw t1, 148(sp)
  lw t0, 156(sp)
  sw t0, 0(t1)
  li t0, 168
  add t0, t0, sp
  sw t0, 164(sp)
  lw t0, 476(sp)
  sw t0, 172(sp)
  lw t1, 164(sp)
  lw t0, 172(sp)
  sw t0, 0(t1)
  li t0, 184
  add t0, t0, sp
  sw t0, 180(sp)
  lw t0, 480(sp)
  sw t0, 188(sp)
  lw t1, 180(sp)
  lw t0, 188(sp)
  sw t0, 0(t1)
  li t0, 200
  add t0, t0, sp
  sw t0, 196(sp)
  lw t0, 484(sp)
  sw t0, 204(sp)
  lw t1, 196(sp)
  lw t0, 204(sp)
  sw t0, 0(t1)
  li t0, 216
  add t0, t0, sp
  sw t0, 212(sp)
  lw t0, 488(sp)
  sw t0, 220(sp)
  lw t1, 212(sp)
  lw t0, 220(sp)
  sw t0, 0(t1)
  li t0, 232
  add t0, t0, sp
  sw t0, 228(sp)
  lw t0, 492(sp)
  sw t0, 236(sp)
  lw t1, 228(sp)
  lw t0, 236(sp)
  sw t0, 0(t1)
  li t0, 248
  add t0, t0, sp
  sw t0, 244(sp)
  lw t0, 496(sp)
  sw t0, 252(sp)
  lw t1, 244(sp)
  lw t0, 252(sp)
  sw t0, 0(t1)
  lw t0, 4(sp)
  lw t0, 0(t0)
  sw t0, 260(sp)
  lw t0, 20(sp)
  lw t0, 0(t0)
  sw t0, 264(sp)
  lw t0, 260(sp)
  lw t1, 264(sp)
  add t0, t0, t1
  sw t0, 268(sp)
  lw t0, 36(sp)
  lw t0, 0(t0)
  sw t0, 276(sp)
  lw t0, 268(sp)
  lw t1, 276(sp)
  add t0, t0, t1
  sw t0, 280(sp)
  lw t0, 52(sp)
  lw t0, 0(t0)
  sw t0, 288(sp)
  lw t0, 280(sp)
  lw t1, 288(sp)
  add t0, t0, t1
  sw t0, 292(sp)
  lw t0, 68(sp)
  lw t0, 0(t0)
  sw t0, 300(sp)
  lw t0, 292(sp)
  lw t1, 300(sp)
  add t0, t0, t1
  sw t0, 304(sp)
  lw t0, 84(sp)
  lw t0, 0(t0)
  sw t0, 312(sp)
  lw t0, 304(sp)
  lw t1, 312(sp)
  add t0, t0, t1
  sw t0, 316(sp)
  lw t0, 100(sp)
  lw t0, 0(t0)
  sw t0, 324(sp)
  lw t0, 316(sp)
  lw t1, 324(sp)
  add t0, t0, t1
  sw t0, 328(sp)
  lw t0, 116(sp)
  lw t0, 0(t0)
  sw t0, 336(sp)
  lw t0, 328(sp)
  lw t1, 336(sp)
  add t0, t0, t1
  sw t0, 340(sp)
  lw t0, 132(sp)
  lw t0, 0(t0)
  sw t0, 348(sp)
  lw t0, 340(sp)
  lw t1, 348(sp)
  add t0, t0, t1
  sw t0, 352(sp)
  lw t0, 148(sp)
  lw t0, 0(t0)
  sw t0, 360(sp)
  lw t0, 352(sp)
  lw t1, 360(sp)
  add t0, t0, t1
  sw t0, 364(sp)
  lw t0, 164(sp)
  lw t0, 0(t0)
  sw t0, 372(sp)
  lw t0, 364(sp)
  lw t1, 372(sp)
  add t0, t0, t1
  sw t0, 376(sp)
  lw t0, 180(sp)
  lw t0, 0(t0)
  sw t0, 384(sp)
  lw t0, 376(sp)
  lw t1, 384(sp)
  add t0, t0, t1
  sw t0, 388(sp)
  lw t0, 196(sp)
  lw t0, 0(t0)
  sw t0, 396(sp)
  lw t0, 388(sp)
  lw t1, 396(sp)
  add t0, t0, t1
  sw t0, 400(sp)
  lw t0, 212(sp)
  lw t0, 0(t0)
  sw t0, 408(sp)
  lw t0, 400(sp)
  lw t1, 408(sp)
  add t0, t0, t1
  sw t0, 412(sp)
  lw t0, 228(sp)
  lw t0, 0(t0)
  sw t0, 420(sp)
  lw t0, 412(sp)
  lw t1, 420(sp)
  add t0, t0, t1
  sw t0, 424(sp)
  lw t0, 244(sp)
  lw t0, 0(t0)
  sw t0, 432(sp)
  lw t0, 424(sp)
  lw t1, 432(sp)
  add t0, t0, t1
  sw t0, 436(sp)
  lw a0, 436(sp)
  lw ra, 0(sp)
  addi sp, sp, 464
  ret

  .text
  .globl main
main:
  addi sp, sp, -112
  sw ra, 0(sp)
  li t0, 1
  sw t0, 36(sp)
  lw a0, 36(sp)
  li t0, 2
  sw t0, 36(sp)
  lw a1, 36(sp)
  li t0, 3
  sw t0, 36(sp)
  lw a2, 36(sp)
  li t0, 4
  sw t0, 36(sp)
  lw a3, 36(sp)
  li t0, 5
  sw t0, 36(sp)
  lw a4, 36(sp)
  li t0, 6
  sw t0, 36(sp)
  lw a5, 36(sp)
  li t0, 7
  sw t0, 36(sp)
  lw a6, 36(sp)
  li t0, 8
  sw t0, 36(sp)
  lw a7, 36(sp)
  call sum
  sw a0, 36(sp)
  li t0, 44
  add t0, t0, sp
  sw t0, 40(sp)
  lw t1, 40(sp)
  lw t0, 36(sp)
  sw t0, 0(t1)
  li t0, 1
  sw t0, 56(sp)
  lw a0, 56(sp)
  li t0, 2
  sw t0, 56(sp)
  lw a1, 56(sp)
  li t0, 3
  sw t0, 56(sp)
  lw a2, 56(sp)
  li t0, 4
  sw t0, 56(sp)
  lw a3, 56(sp)
  li t0, 5
  sw t0, 56(sp)
  lw a4, 56(sp)
  li t0, 6
  sw t0, 56(sp)
  lw a5, 56(sp)
  li t0, 7
  sw t0, 56(sp)
  lw a6, 56(sp)
  li t0, 8
  sw t0, 56(sp)
  lw a7, 56(sp)
  li t0, 9
  sw t0, 56(sp)
  lw t0, 56(sp)
  sw t0, 4(sp)
  li t0, 10
  sw t0, 56(sp)
  lw t0, 56(sp)
  sw t0, 8(sp)
  li t0, 11
  sw t0, 56(sp)
  lw t0, 56(sp)
  sw t0, 12(sp)
  li t0, 12
  sw t0, 56(sp)
  lw t0, 56(sp)
  sw t0, 16(sp)
  li t0, 13
  sw t0, 56(sp)
  lw t0, 56(sp)
  sw t0, 20(sp)
  li t0, 14
  sw t0, 56(sp)
  lw t0, 56(sp)
  sw t0, 24(sp)
  li t0, 15
  sw t0, 56(sp)
  lw t0, 56(sp)
  sw t0, 28(sp)
  li t0, 16
  sw t0, 56(sp)
  lw t0, 56(sp)
  sw t0, 32(sp)
  call sum2
  sw a0, 56(sp)
  li t0, 64
  add t0, t0, sp
  sw t0, 60(sp)
  lw t1, 60(sp)
  lw t0, 56(sp)
  sw t0, 0(t1)
  lw t0, 40(sp)
  lw t0, 0(t0)
  sw t0, 76(sp)
  lw t0, 60(sp)
  lw t0, 0(t0)
  sw t0, 80(sp)
  lw t0, 76(sp)
  lw t1, 80(sp)
  add t0, t0, t1
  sw t0, 84(sp)
  lw a0, 84(sp)
  lw ra, 0(sp)
  addi sp, sp, 112
  ret
