#pragma once
#include<iostream>
#include<cstring>
#include<memory>
#include<cassert>
#include<vector>
#include "util.h"

using namespace std;

static SymbolTable symbol_table_init;//init table
static SymbolTable* symbol_table;//current
static LoopInfo* loop_info;

static vector<int> *init_array(const vector<int>*array_types,int k,const vector<unique_ptr<BaseAST> > *init_values);

// CompUnit 是 BaseAST
class CompUnitAST : public BaseAST {
 public:
    vector<unique_ptr<BaseAST>> var_def_list;  //全局变量
    vector<unique_ptr<BaseAST>> func_def_list; //函数定义

    KIR Dump() const override {
        if (debug) {
            cout << "CompUnitAST Dumping" << endl;
        }
        symbol_table = &symbol_table_init;
        loop_info = NULL;

        string s = "decl @getint(): i32\n";
        s += "decl @getch(): i32\n";
        s += "decl @getarray(*i32): i32\n";
        s += "decl @putint(i32)\n";
        s += "decl @putch(i32)\n";
        s += "decl @putarray(i32, *i32)\n";
        s += "decl @starttime()\n";
        s += "decl @stoptime()\n\n"; //内置函数
        //将内置函数插入符号表
        symbol_table_init.Insert("getint", SymbolTableEntry(3));
        symbol_table_init.Insert("getch", SymbolTableEntry(3));
        symbol_table_init.Insert("getarray", SymbolTableEntry(3));
        symbol_table_init.Insert("putint", SymbolTableEntry(2));
        symbol_table_init.Insert("putch", SymbolTableEntry(2));
        symbol_table_init.Insert("putarray", SymbolTableEntry(2));
        symbol_table_init.Insert("starttime", SymbolTableEntry(2));
        symbol_table_init.Insert("stoptime", SymbolTableEntry(2));

        for (auto &var_def : var_def_list) {
            s += var_def->Dump().s;
        }
        for (auto &func_def : func_def_list) {
            s += func_def->Dump().s;
        }
        return KIR(s);
    }
};

// FuncDef 也是 BaseAST

class FuncDefAST : public BaseAST {
 public:
  string func_type;
  string ident;
  unique_ptr<BaseAST> block;
  vector<unique_ptr<BaseAST> > param_list;
  KIR Dump() const override {
    string s = func_type;
    if(debug){
        cout<<"FuncdefAST"<<endl;
        cout<<"func type : "<<s<<endl;
    }
    string type_id = "";
    if(s == "int")
        type_id = ": i32";
    if(s == "void")
        symbol_table_init.Insert(ident,SymbolTableEntry(2));
    else     
        symbol_table_init.Insert(ident,SymbolTableEntry(3));
    tmp_var_cnt = 0;

    SymbolTable* new_symbol_table = new SymbolTable(symbol_table);
    symbol_table = new_symbol_table;

    string p_ir = "";
    string p_gen_ir = "";
    int flag = 1;
    for(auto &i: param_list){
        auto p_dump = i->Dump();
        p_gen_ir += p_dump.s;
        if(flag == 1){
            flag = 0;
            p_ir += p_dump.tmp_var + ":"+ p_dump.type;
        }
        else{
            p_ir += ", " + p_dump.tmp_var + ":" +p_dump.type;
        }
    }

    KIR block_dump = block->Dump();
    string tmp_ir = block_dump.s;
    if(block_dump.end == 0){
        if(s == "int")
            tmp_ir += "  ret 0\n";
        else{
            tmp_ir += "  ret\n";
        }
    }
    symbol_table = symbol_table->parent;
    return KIR("fun @" + ident + "(" + p_ir + ")" + type_id + "{\n%entry:\n" + p_gen_ir +tmp_ir + "}\n");
  }
    
};

class FuncFParamAST : public BaseAST {
public:
    unique_ptr<BaseAST> type;
    string id;
    vector<unique_ptr<BaseAST>> array_type;
    int kind;
    KIR Dump() const override {
        if (debug) {
            cout << "FuncFParamAST Dumping" << endl;
        }

        switch (kind) {
            case 0: {
                string t = type->Dump().s;                                        // i32
                string arg_name = "@" + id + "_" + to_string(symbol_table->num); //@x_1
                string tmp_var = "%" + to_string(tmp_var_cnt++);                      //%1
                string ret_ir = "  " + tmp_var + " = alloc " + t + "\n";            //%1 = alloc i32
                ret_ir += "  store " + arg_name + ", " + tmp_var + "\n";             //store @x_1, %1
                symbol_table->Insert(id, SymbolTableEntry(1, tmp_var));             //x->%1

                return KIR(ret_ir, arg_name);
            }
            case 1: {
                string t = "*" + get_type_name(&array_type);                         // *i32
                string arg_name = "@" + id + "_" + to_string(symbol_table->num); //@x_1
                string tmp_var = "%" + to_string(tmp_var_cnt++);                      //%1
                string ret_ir = "  " + tmp_var + " = alloc " + t + "\n";            //%1 = alloc *i32
                ret_ir += "  store " + arg_name + ", " + tmp_var + "\n";             //store @x_1, %1
                auto array_types = new vector<int>(); //每一维的长度
                for (auto &arrayType : array_type) {
                    array_types->push_back(arrayType->get_value());
                }
                symbol_table->Insert(id, SymbolTableEntry(5, array_types, tmp_var));
                KIR ret = KIR(ret_ir, arg_name);
                ret.type = t;
                return ret;
            }
            default:
                assert(false);
        }
    }

};

class BlockAST:public BaseAST{
public:
    vector<unique_ptr<BaseAST> > block_items;
    BlockAST(){
        block_items.clear();
    }
    KIR Dump() const override{
        if(debug){
            cout<<"BlockAST built, length : "<< block_items.size() <<endl;
        }
        SymbolTable *new_symbol_table = new SymbolTable(symbol_table);
        symbol_table = new_symbol_table;

        string tmp = "";
        KIR ret;
        for(auto &i : block_items){
            KIR block_ir = i->Dump();
            tmp += block_ir.s;
            if(block_ir.end == 1){
                ret.end = 1;
                break;
            }
        }
        ret.s = tmp;
        symbol_table = symbol_table->parent;
        free(new_symbol_table);
        return ret;
    }
};

class BlockItemAST : public BaseAST {
    public:
        int kind;//0-decl,1-stmt
        unique_ptr<BaseAST> decl;
        unique_ptr<BaseAST> stmt;
        KIR Dump() const override {
            if(kind == 0){
                if(debug){
                    cout << "decl dumping" << endl;
                }
                return decl->Dump();
            }
            else if(kind == 1){
                if(debug){
                    cout << "stmt dumping" << endl;
                }
                return stmt->Dump();
            }
            else{
                if(debug){
                    cout << "blockitemAST error" << endl;
                }
                assert(0);
            }
        }
};

class StmtAST : public BaseAST{
public:
    //std::unique_ptr<BaseAST> number;
    unique_ptr<BaseAST> matched_stmt;
    unique_ptr<BaseAST> other_stmt;
    int kind;

    StmtAST() = default;
    KIR Dump() const override{
        if(debug){
            cout<<"building stmt"<<endl;
        }
        if(kind == 0){
            if(debug){
            cout<<"building Matchedstmt"<<endl;
            }
            return matched_stmt->Dump();
        }
        else if(kind == 1){
            if(debug){
            cout<<"building Otherstmt"<<endl;
            }
            return other_stmt->Dump();
        }
        else{
            if(debug){
                cout<<"building stmt error,unknown type"<<endl;
            }
            return KIR("unknown stmt");
        }
    }
};

class MatchedStmtAST:public BaseAST{
    public:
    unique_ptr<BaseAST> exp;
    unique_ptr<BaseAST> lval;
    unique_ptr<BaseAST> block;
    unique_ptr<BaseAST> matched_stmt_1;
    unique_ptr<BaseAST> matched_stmt_2;
    int kind;
    //kind = 0 is exp
    KIR Dump() const override{
        if(kind == 0){
            if(debug){
                cout<<"Matchedstmt, kind 0, exp"<<endl;
            }
            auto exp_dump = exp->Dump();
            string tmp_s = exp_dump.s;
            string tmp_var = exp_dump.tmp_var;
            return KIR(tmp_s + " ret " + tmp_var + "\n",1);//end of control stream
        }
        else if(kind == 1){
            if(debug){
                cout<<"MatchedStmtAST,kind 1, lval"<<endl;
            }
            KIR exp_dump = exp->Dump();
            string tmp_ir = exp_dump.s;
            string tmp_var = exp_dump.tmp_var;

            KIR lval_dump = lval->get_var_addr();
            tmp_ir.append(lval_dump.s);
            return KIR(tmp_ir + "  store " + tmp_var + ", " + lval_dump.tmp_var + "\n");
        }
        else if(kind == 2){
            if(debug){
                cout<<"MatchedStmtAST, kind 2,block "<<endl;
            }
            return block->Dump();
        }
        else if(kind == 3){
            if(debug){
                cout<<"MatchedStmtAST, kind 3,Exp "<<endl;
            }
            return exp->Dump();
        }
        else if(kind == 4){
            if(debug){
                cout<<"MatchedStmtAST, kind 4,empty "<<endl;
            }
            return KIR("");
        }
        else if(kind == 5){
            if(debug){
                cout<<"MatchedStmtAST, kind 5,if "<<endl;
            }
            KIR exp_dump = exp->Dump();
            string tmp_ir = exp_dump.s;
            string tmp_var = exp_dump.tmp_var;
            string true_label = new_label();
            string false_label = new_label();
            string end_label = new_label();
            
            KIR ret_1 = matched_stmt_1->Dump();
            KIR ret_2 = matched_stmt_2->Dump();

            string s = tmp_ir + "  br " + tmp_var + ", " + true_label 
                     + ", " + false_label + true_label + ":\n" + ret_1.s;
            if(ret_1.end == 0){
                s += "  jump " + end_label + "\n";
            }
            s += false_label + ":\n" + ret_2.s;
            if(ret_2.end == 0){
                s += "  jump " + end_label + "\n";
            }
            s += end_label + ":\n";
            return KIR(s);
        }
        else if(kind == 6){
            if(debug){
                cout<<"MatchedStmtAST, kind 6,break "<<endl;
            }
            if(loop_info == nullptr){
                if(debug){
                    cout<<"break without while"<<endl;
                }
                assert(0);
            }
            else{
                string s = "  jump " + loop_info->end_label + "\n";
                return KIR(s,1);
            }
        }
        else if(kind == 7){
            if(debug){
                cout<<"MatchedStmtAST, kind 7,continue "<<endl;
            }
            if(loop_info == nullptr){
                if(debug){
                    cout<<"continue without while"<<endl;
                }
                assert(0);
            }
            else{
                string s = "  jump " + loop_info->start_label + "\n";
                return KIR(s,1);
            }
        }
        else if(kind == 8){
            if(debug){
                cout<<"MatchedStmtAST, kind 8,while "<<endl;
            }
            KIR exp_dump = exp->Dump();
            string tmp_ir = exp_dump.s;
            string tmp_var =exp_dump.tmp_var;
            string while_start = new_label();
            string while_body = new_label();
            string end_label = new_label();

            LoopInfo* tmp_loop = new LoopInfo(while_start,end_label,loop_info);
            loop_info = tmp_loop;

            KIR matched_stmt_dump = matched_stmt_1->Dump();
            //循环起始
            string s = "  jump " + while_start + "\n";
            s += while_start + ":\n" + tmp_ir + "  br " + tmp_var + ", " + while_body + ", " + end_label + "\n";

            //循环体
            s += while_body + ":\n" + matched_stmt_dump.s;
            if(matched_stmt_dump.end == 0){
                s += "  jump " + while_start + "\n";
            }

            s += end_label + ":\n";

            loop_info = loop_info->parent;
            
            return KIR(s);
        }
        else{
            if(debug){
                cout<<"MatchedStmtAST, kind unknown!"<<endl;
            }
            assert(0);
        }
    }
};

class OtherStmtAST : public BaseAST {
public:
    int kind;
    unique_ptr<BaseAST> exp;
    unique_ptr<BaseAST> other_stmt;
    unique_ptr<BaseAST> matched_stmt;
    unique_ptr<BaseAST> stmt;

    KIR Dump() const override {
        if(kind == 0){
            if(debug){
                cout<<"OtherStmtAST, kind 0, if exp"<<endl;
            }
            KIR exp_dump = exp->Dump();
            string tmp_ir = exp_dump.s;
            string tmp_var = exp_dump.tmp_var;
            string true_label = new_label();
            string false_label = new_label();
            string end_label = new_label();
            
            KIR ret = stmt->Dump();
            string s = tmp_ir + "  br " + tmp_var + ", " + true_label + ", " + false_label + "\n";
            s += true_label + ":\n" + ret.s;
            if(ret.end == 0){
                s += "  jump " + end_label + "\n";
            }
            s += false_label + ":\n" + "  jump " + end_label + "\n";

            s += end_label + ":\n";
            return KIR(s);
        }
        else if(kind == 1){
            if(debug){
                cout<<"OtherStmtAST, kind 1, if exp matchedstmt otherstmt"<<endl;
            }
            KIR exp_dump = exp->Dump();
            string tmp_ir = exp_dump.s;
            string tmp_var = exp_dump.tmp_var;
            string true_label = new_label();
            string false_label = new_label();
            string end_label = new_label();
            
            KIR ret_1 = matched_stmt->Dump();
            KIR ret_2 = other_stmt->Dump();
            string s = tmp_ir + "  br " + tmp_var + ", " + true_label + ", " + false_label + "\n";
            s += true_label + ":\n" + ret_1.s;
            if(ret_1.end == 0){
                s += "  jump " + end_label + "\n";
            }
            s += false_label + ":\n" + ret_2.s;
            if(ret_2.end == 0){
                s += "  jump " + end_label + "\n";
            }
            s += end_label + ":\n";
            return KIR(s);
        }
        else if(kind == 2){
            if(debug){
                cout<<"OtherStmtAST, kind 2"<<endl;
            }
            KIR exp_dump = exp->Dump();
            string tmp_ir = exp_dump.s;
            string tmp_var = exp_dump.tmp_var;
            string while_start = new_label();
            string while_body = new_label();
            string end_label = new_label();

            LoopInfo* tmp_loop = new LoopInfo(while_start,end_label,loop_info);
            loop_info = tmp_loop;
            
            KIR other_stmt_dump = other_stmt->Dump();
            //循环起始
            string s = "  jump " + while_start + "\n";
            s += while_start + ":\n" + tmp_ir + "  br " +tmp_var +", " + while_body + ", " + end_label + "\n";
            //循环体
            s += while_body + ":\n" + other_stmt_dump.s;
            if(other_stmt_dump.end == 0){
                s += "  jump " + while_start + "\n";
            }
            s += end_label + ":\n";
            loop_info = loop_info->parent;
            return KIR(s);
        }
        else{
            if(debug){
                cout<<"OtherStmtAST, kind unknown!"<<endl;
            }
            assert(0);
        }
    }

};

class LValAST : public BaseAST {
public:
    string id = "error";
    vector<unique_ptr<BaseAST> > array_index;
    LValAST(){};
    LValAST(string _i){
        this->id = _i;
    }
    KIR Dump() const override {
        string tmp_var = "%" + to_string(tmp_var_cnt++);
        //cout<<1111<<endl;
        SymbolTableEntry* entry = symbol_table->find(id);
        //cout<<111<<endl;
        if(entry != nullptr){
            //cout<<entry->kind<<endl;
            if(entry->kind == 1){
                //cout<<"1"<<endl;
                if(debug){
                    cout<<"LValAST, entry->kind 1\n";
                    cout<<"id : "<<id<<endl;
                    cout<< "entry addr: "<< entry->addr <<endl;
                }
                return KIR("  " + tmp_var + " = load " + entry->addr + "\n", tmp_var);
            }
            else if(entry->kind == 0){
                //cout<<"0"<<endl;
                if(debug){
                    cout<<"LValAST, entry->kind 0\n";
                }
                return KIR("",to_string(entry->constant));
            }
            else if(entry->kind == 4){
                string tmp_cmd = "";
                int k = array_index.size();
                string last_name = entry->addr;
                for(int j = k - 1; j >=0; --j){
                    string this_name = "%" + to_string(tmp_var_cnt++);
                    KIR j_dump = array_index[j]->Dump();
                    tmp_cmd += j_dump.s;
                    tmp_cmd.append("  " + this_name + " = getelemptr "+ last_name + ", "+ j_dump.tmp_var + "\n");
                    last_name = this_name;
                }
                if(k == entry->array_types->size()){
                    tmp_cmd.append("  " + tmp_var + " = load " + last_name + "\n");
                }
                else{
                    tmp_cmd.append("  "+ tmp_var + " = getelemptr " + last_name + ", 0\n");
                }
                return KIR(tmp_cmd,tmp_var);
            }
            else if(entry->kind == 5){
                string tmp_cmd = "";
                int k = array_index.size();
                string tmp_var_2 = "%" + to_string(tmp_var_cnt++);
                tmp_cmd.append("  " + tmp_var_2 + " = load " + entry->addr + "\n");
                if(k>=1){
                    string last_name = tmp_var_2;
                    string tmp_var_3 = "%" + to_string(tmp_var_cnt++);
                    KIR k_dump = array_index[k - 1]->Dump();
                    tmp_cmd += k_dump.s;
                    tmp_cmd.append("  "+tmp_var_3 + " = getptr " + tmp_var_2 + ", " + k_dump.tmp_var + "\n");
                    last_name = tmp_var_3;
                    for(int j = k-2;j >= 0;--j){
                        string this_name = "%" + to_string(tmp_var_cnt++);
                        KIR j_dump = array_index[j]->Dump();
                        tmp_cmd += j_dump.s;
                        tmp_cmd.append("  " + this_name + " = getelemptr " + last_name + ", "+ j_dump.tmp_var+"\n");
                        last_name = this_name;
                    }
                    if(k == entry->array_types->size() + 1){
                        tmp_cmd.append("  " + tmp_var + " = load " + last_name + "\n");
                    }
                    else{
                        tmp_cmd.append("  "+ tmp_var + " = getelemptr " + last_name + ", 0\n");
                    }
                    return KIR(tmp_cmd,tmp_var);
                }
                else{
                    return KIR(tmp_cmd,tmp_var_2);
                }
            }
            else{
                //cout<<"else"<<endl;
                if(debug){
                    cout<<"LValAST, wrong var type\n";
                }
                assert(0);
            }
        }
        else{
            if(debug){
                cout<< "find nothing!"<<endl;
            }
            return KIR("  " + tmp_var + " = load @" + id + "\n",tmp_var);
        }
    }
    virtual KIR get_var_addr() override {
        SymbolTableEntry* entry = symbol_table->find(id);
        if(entry==nullptr){
            return KIR("","errNoAddr");
        }
        if(entry->kind == 1){
            return KIR("",entry->addr);
        }
        else if(entry->kind == 4){
            string tmp_cmd = "";
            int k = array_index.size();
            string last_name = entry->addr;
            for(int j = k-1;j>=0;--j){
                string this_name = "%" + to_string(tmp_var_cnt++);
                KIR j_dump = array_index[j]->Dump();
                tmp_cmd += j_dump.s;
                tmp_cmd.append("  " + this_name + " = getelemptr " + last_name + ", " + j_dump.tmp_var + "\n");
                last_name = this_name;
            }
            return KIR(tmp_cmd, last_name);
        }
        else if(entry->kind == 5){
            string tmp_cmd = "";
            int k = array_index.size();
            string tmp_var_2 = "%" + to_string(tmp_var_cnt++);
            tmp_cmd.append("  " + tmp_var_2 + " = load " + entry->addr + "\n");
            string last_name = tmp_var_2;
            if(k >= 1){
                string tmp_var_h = "%"+to_string(tmp_var_cnt++);
                KIR k_dump = array_index[k-1]->Dump();
                tmp_cmd += k_dump.s;
                tmp_cmd.append("  " + tmp_var_h + " = getptr " + tmp_var_2 + ", " + k_dump.tmp_var + "\n");
                last_name = tmp_var_h;
            }
            for(int j = k-2;j>=0;--j){
                string this_name = "%" + to_string(tmp_var_cnt++);
                KIR j_dump = array_index[j]->Dump();
                tmp_cmd += j_dump.s;
                tmp_cmd.append("  " + this_name + " = getelemptr " + last_name + ", " + j_dump.tmp_var + "\n");
                last_name = this_name;
            }
            return KIR(tmp_cmd,last_name);
        }
        else{
            if(debug){
                cout<< " get_addr_error, kind :" << entry->kind<<endl;
            }
            assert(0);
        }
    }
    virtual int get_value() const override {
        SymbolTableEntry *entry = symbol_table->find(id);
        if(entry != nullptr && entry->kind == 0){
            return entry->constant;
        }
        else{
            return get_value_err;
        }
    }
};

class ConstExpAST : public BaseAST {
public:
    unique_ptr<BaseAST> exp;
    KIR Dump() const override{
        return exp->Dump();
    }
    virtual int get_value() const override{
        return exp->get_value();
    }
};

class VarExpAST : public BaseAST {
public:

};

class ConstDeclAST : public BaseAST {
    public:
        unique_ptr<BaseAST> btype;
        vector<unique_ptr<BaseAST> > const_defs;
        KIR Dump() const override {
            string tmp_cmd = "";
            string type = btype->Dump().s;
            for(auto &i: const_defs){
                KIR const_dump = i->Dump();
                tmp_cmd.append(const_dump.s);
            }
            return KIR(tmp_cmd,"");
        }
};

class BTypeAST : public BaseAST {
    public:
        int kind = 0;
        KIR Dump() const override {
            if(kind==0){
                return KIR("i32");
            }
            else{
                if(debug)
                {
                    cout<<"Btype error!"<<endl;
                }
                assert(0);
            }
        }
};

class ConstDefAST : public BaseAST {
    public:
    string id;
    vector<unique_ptr<BaseAST> > array_types;
    unique_ptr<BaseAST> exp;
    KIR Dump() const override {
        if(debug){
            cout<< " ConstDefAST, id : " << id <<endl;
        }
        if(array_types.size() == 0){
            int val = exp->get_value();
            SymbolTableEntry tmp_entry(0,val);
            //cout<<"kind:"<<tmp_entry.kind << "val:" <<tmp_entry.constant<<endl;
            symbol_table->Insert(id,tmp_entry);
            string s = id + " = " + to_string(val) + "\n";
            return KIR("",id);
        }
        else{
            string block_num = "_" + to_string(symbol_table->num);
            string tmp_var = "@" + id + block_num;
            auto tmp_array_types = new vector<int>();
            for(auto&array_type: array_types){
                tmp_array_types->push_back(array_type->get_value());
            }
            KIR exp_dump = exp->Dump(tmp_array_types);
            string type = get_type_name(&array_types);
            if(block_cnt < 1){
                string tmp_cmd = exp_dump.s;
                tmp_cmd.append("global " + tmp_var + " = alloc " + type + ", " + exp_dump.tmp_var + "\n");
                symbol_table->Insert(id,SymbolTableEntry(4,tmp_array_types,tmp_var));
                return KIR(tmp_cmd,tmp_var);
            }
            else{
                string tmp_cmd = exp_dump.s;
                tmp_cmd.append("  " + tmp_var + " = alloc " + type + "\n");
                vector<int> *array_val = exp_dump.array_val;
                tmp_cmd.append(store_array(tmp_var,tmp_array_types,array_val));
                symbol_table->Insert(id,SymbolTableEntry(4,tmp_array_types,tmp_var));
                return KIR(tmp_cmd,tmp_var);
            }
        }
    }
};

class ConstInitValAST : public BaseAST {
    public:
    int kind;//0 - constexp;
    unique_ptr<BaseAST> const_exp;
    vector<unique_ptr<BaseAST> > const_init_vals;
    KIR Dump() const override{
        if(kind == 0)
        {
            return const_exp->Dump();
        }
        else{
            if(debug){
                cout<< "kind error of const init val ast"<<endl;
            }
            assert(0);
        }
    }
    
    virtual int get_value() const override {
        if(debug){
            cout<<"const init val ast get value"<<endl;
        }
        return const_exp->get_value();
    }
    KIR Dump(const vector<int> *array_types) const override {
        int k = array_types->size();
        if(kind == 0){
            KIR exp_dump = const_exp->Dump();
            return exp_dump;
        }
        else if(kind == 1){
            vector<int> *array_val = init_array(array_types, k - 1,&const_init_vals);
            KIR s = KIR("");
            string tmp_var = int_to_string(array_types,array_val,k-1);
            s.array_val = array_val;
            s.tmp_var = tmp_var;
            return s;
        }
        else if(kind == 2){//初始化为空值
            vector<int> *array_val = init_array(array_types, k - 1,&const_init_vals);
            KIR s = KIR("");
            string tmp_var = int_to_string(array_types,array_val,k-1);
            s.array_val = array_val;
            s.tmp_var = tmp_var;
            return s;
        }
        else{
            if(debug){
                cout<<"error: ConstInitval array dump"<<endl;
            }
            assert(0);
        }
    }
};

class VarDeclAST : public BaseAST {
    public:
    unique_ptr<BaseAST> btype;
    vector<unique_ptr<BaseAST> > var_defs;

    KIR Dump() const override {
        string tmp_cmd = "";
        string type = btype->Dump().s;
        for(auto &i : var_defs){
            tmp_cmd.append(i->Dump().s);
        }
        return KIR(tmp_cmd,"");
    }
};

class VarDefAST : public BaseAST {
    public:
    int kind;//0 - without init val,1 - with
    string id;
    vector<unique_ptr<BaseAST> > array_types;
    unique_ptr<BaseAST> exp;

    KIR Dump() const override {
        string block_num = "_" + to_string(symbol_table->num);
        string tmp_var = "@" + id + block_num;

        auto tmp_array_types = new vector<int>();
        for(auto &i: array_types){
            tmp_array_types->push_back(i->get_value());
        }
        if(kind == 0){
            if(debug){
                cout<<"VarDefAST dump ,kind :" <<kind<<endl;
            }
            string type = get_type_name(&array_types);
            if(block_cnt < 1){
                string tmp_cmd = "global " + tmp_var + " = alloc " + type + ", zeroinit\n";
                if(type == "i32"){
                    symbol_table->Insert(id,SymbolTableEntry(1,tmp_var));
                }
                else{
                    symbol_table->Insert(id,SymbolTableEntry(4,tmp_array_types,tmp_var));
                }
                return KIR(tmp_cmd,tmp_var);
            }
            else{
                string tmp_cmd = "  " + tmp_var + " = alloc " + type + "\n";
                if(type == "i32"){
                    symbol_table->Insert(id,SymbolTableEntry(1,tmp_var));
                }
                else{
                    symbol_table->Insert(id,SymbolTableEntry(4,tmp_array_types,tmp_var));
                }
                return KIR(tmp_cmd,tmp_var);
            }
        }
        else if(kind == 1){
            KIR exp_dump = exp->Dump(tmp_array_types);
            string type = get_type_name(&array_types);
            if(block_cnt < 1){
                string tmp_cmd = exp_dump.s;
                tmp_cmd.append("global " + tmp_var + " = alloc " + type + ", " + exp_dump.tmp_var + "\n");
                if(type == "i32"){
                    symbol_table->Insert(id,SymbolTableEntry(1,tmp_var));
                }
                else{
                    symbol_table->Insert(id,SymbolTableEntry(4,tmp_array_types,tmp_var));
                }
                return KIR(tmp_cmd,tmp_var);
            }
            else{
                string tmp_cmd = exp_dump.s;
                tmp_cmd.append("  " + tmp_var + " = alloc " + type + "\n");
                if(array_types.size()==0){
                    tmp_cmd.append("  store " + exp_dump.tmp_var + ", " + tmp_var + "\n");
                }
                else{
                    vector<int> *array_val = exp_dump.array_val;
                    tmp_cmd.append(store_array(tmp_var,tmp_array_types,array_val));
                }
                if(type == "i32"){
                    symbol_table->Insert(id,SymbolTableEntry(1,tmp_var));
                }
                else{
                    symbol_table->Insert(id,SymbolTableEntry(4,tmp_array_types,tmp_var));
                }
                return KIR(tmp_cmd,tmp_var);
            }

        }
        else{
            if(debug){
                cout << "VarDefAST dump error"<<endl;
            }
            assert(0);
        }
    }
};

class InitValAST : public BaseAST {
    public:
        int kind;//0-exp,1-{initvallist},2-{}
        unique_ptr<BaseAST> exp;
        vector<unique_ptr<BaseAST> > init_vals;
        virtual int get_value() const override {
            if(debug){
            cout<<"init val ast get value"<<endl;
            }
            return exp->get_value();
        }
        KIR Dump() const override{
            return exp->Dump();
        }
        KIR Dump(const vector<int> *array_types) const override {
            int k = array_types->size();
            if(kind == 0){
                KIR exp_dump = exp->Dump();
                return exp_dump;
            }
            else if(kind == 1){
                vector<int> *array_val = init_array(array_types,k-1,&init_vals);
                KIR s = KIR("");
                string tmp_var = int_to_string(array_types,array_val,k-1);
                s.array_val = array_val;
                s.tmp_var = tmp_var;
                return s;
            }
            else if(kind == 2){//无初始化值
                vector<int> *array_val = init_array(array_types,k-1,&init_vals);
                KIR s = KIR("");
                string tmp_var = int_to_string(array_types,array_val,k-1);
                s.array_val = array_val;
                s.tmp_var = tmp_var;
                return s;
            }
            else{
                if(debug){
                    cout<<"array type error!"<<endl;
                }
                assert(0);
            }
        }
};

class ExpAST : public BaseAST{
public:
    unique_ptr<BaseAST> exp;
    KIR Dump() const override{
        if(debug){
            cout<<"ExpAST"<<endl;
        }
        int value = exp->get_value();
        if(value != get_value_err){
            return KIR("",to_string(value));
        }
        else return exp->Dump();
    }
    virtual int get_value() const override {
        if(debug){
            cout<<"getting Value"<<endl;
        }
        int value = exp->get_value();
        if(debug){
            cout<<"get value of exp, value: "<<value<<endl;
        }
        return value;
    }
};

class LOrExpAST : public BaseAST{
public:
    int kind;
    unique_ptr<BaseAST> lor_exp;// kind = 1,lor || land
    unique_ptr<BaseAST> land_exp;//kind = 0
    
    KIR generate_return() const{
        string true_label = new_label();
        string false_label = new_label();
        string end_label = new_label();
        string tmp_var_1 = "%" + to_string(tmp_var_cnt);
        tmp_var_cnt++;
        string s = " " + tmp_var_1 + " = alloc i32\n  store 1, " + tmp_var_1 + "\n";
        
        KIR lor_dump = lor_exp->Dump();
        KIR land_dump = land_exp->Dump();
        
        s += lor_dump.s;

        string tmp_var_2 = "%" + to_string(tmp_var_cnt);
        tmp_var_cnt++;
        s += "  " + tmp_var_2 + " = eq " + lor_dump.tmp_var + ", 0\n";
        s += "  br " + tmp_var_2 + ", " + true_label + ", " + false_label +"\n";
        
        s += true_label + ":\n";

        s += land_dump.s;
        string tmp_var_3 = "%" + to_string(tmp_var_cnt);
        tmp_var_cnt++;
        s += " " + tmp_var_3 + " = ne " + land_dump.tmp_var + ", 0\n";
        s += "  store " + tmp_var_3 + ", " +tmp_var_1 + "\n";
        
        string tmp_var_4 = "%" + to_string(tmp_var_cnt);
        tmp_var_cnt++;
        s += "  jump " + end_label + "\n" + false_label + ":\n  jump "+ end_label 
            + "\n" + end_label + ":\n";

        s += "  " + tmp_var_4 + " = load " + tmp_var_1 + "\n";
        return KIR(s,tmp_var_4);
    }
    KIR Dump() const override{
        if(kind == 0){
            if(debug){
                cout<<"lorexpAST, kind 0"<<endl;
            }
            return land_exp->Dump();
        }
        else if(kind == 1){
            if(debug){
                cout<<"lor||land,kind 1"<<endl;
            }
            return generate_return();
        }
        else{
            assert(false);
        }
    }

    virtual int get_value() const override {
        if(kind == 0){
            if(debug){
            cout<<"lor exp ast get value,kind 0"<<endl;
            }
            return land_exp->get_value();
        }
        else if(kind==1){
            if(debug){
            cout<<"lor exp ast get value,kind 1"<<endl;
            }
            int left = lor_exp->get_value();
            int right = land_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left || right;
            }
        }
        else{
            if(debug){
                cout<<"lorexpast get value error"<<endl;
            }
            assert(0);
        }
    }
};

class LAndExpAST : public BaseAST{
public:
    int kind;
    unique_ptr<BaseAST> land_exp; // kind = 1, land && eq
    unique_ptr<BaseAST> eq_exp; // kind = 0
    
    KIR generate_return() const{
        string true_label = new_label();                                            
        string false_label = new_label();                                         
        string end_label = new_label();                                          
        string tmp_var_1 = "%" + to_string(tmp_var_cnt++);                    
        string s = "  " + tmp_var_1 + " = alloc i32\n  store 0, " + tmp_var_1 + "\n"; 

        KIR land_dump = land_exp->Dump();
        KIR eq_dump = eq_exp->Dump();

        s += land_dump.s; 

        string tmp_var_2 = "%" + to_string(tmp_var_cnt++);               
        s += "  " + tmp_var_2 + " = ne " + land_dump.tmp_var + ", 0\n"; 

        s += "  br " + tmp_var_2 + ", " + true_label + ", " + false_label
              + "\n";            
        s += true_label + ":\n";

        s += eq_dump.s;
        string tmp_var_3 = "%" + to_string(tmp_var_cnt++);               
        s += "  " + tmp_var_3 + " = ne " + eq_dump.tmp_var + ", 0\n"; 
        s += "  store " + tmp_var_3 + ", " + tmp_var_1 + "\n";            

        string tmp_var_4 = "%" + to_string(tmp_var_cnt++); 
        s += "  jump " + end_label + "\n" + false_label + ":\n  jump " + end_label + "\n"
              + end_label + ":\n";

        s += "  " + tmp_var_4 + " = load " + tmp_var_1 + "\n"; //%4=%1
        
        return KIR(s, tmp_var_4);
    }
    KIR Dump() const override{
        if(kind == 0){
            if(debug){
                cout<<"landexpAST, kind 0"<<endl;
            }
            return eq_exp->Dump();
        }
        else if(kind == 1){
            if(debug){
                cout<<"landexpAST, kind 1"<<endl;
            }
            return generate_return();
        }
        else{
            assert(0);
        }
    }
    virtual int get_value() const override {
        if(kind == 0){
            if(debug){
            cout<<"land exp ast get value,kind 0"<<endl;
            }
            return eq_exp->get_value();
        }
        else if(kind == 1){
            if(debug){
            cout<<"land exp ast get value,kind 1"<<endl;
            }
            int left = land_exp->get_value();
            int right = eq_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left && right;
            }
        }
        else{
            if(debug){
                cout<< "landExpAST get value error"<<endl;
            }
            assert(0);
        }
    }
};

class EqExpAST : public BaseAST{
public:
    int kind;
    //0 rel,1 == ,2 !=
    unique_ptr<BaseAST> eq_exp;
    unique_ptr<BaseAST> rel_exp;
    KIR generate_return() const{
        KIR exp_dump_1 = eq_exp->Dump();
        string tmp_cmd_1 = exp_dump_1.s;
        string tmp_var_1 = exp_dump_1.tmp_var;
        KIR exp_dump_2 = rel_exp->Dump();
        string tmp_cmd_2 = exp_dump_2.s;
        string tmp_var_2 = exp_dump_2.tmp_var;
        string tmp_var_new = "%" + to_string(tmp_var_cnt++);
        if (debug) {
            cout << "CMD1: " << tmp_cmd_1<< endl;
            cout << "Var1: %" << tmp_var_1<< endl;
            cout << "CMD2: " << tmp_cmd_2 << endl;
            cout << "Var2: %" << tmp_var_2 << endl;
            cout << "VarNew: " << tmp_var_new << endl;
        }
        string op = " = eq ";
        if (kind == 1)
            op = " = eq ";
        else if (kind == 2)
            op = " = ne ";
        KIR ret = KIR(
            tmp_cmd_1 + tmp_cmd_2 + "  " + tmp_var_new + op + tmp_var_1 + ", " + tmp_var_2 + "\n", tmp_var_new);
        return ret;
    }
    KIR Dump() const override{
        if(kind == 0){
            if(debug){
                cout<<"eq_Exp_AST, kind 0"<<endl;
            }
            return rel_exp->Dump();
        }
        else if(kind == 1){
            if(debug){
                cout<<"eq_Exp_AST, kind 1"<<endl;
            }
            return generate_return();
        }
        else if(kind==2){
            if(debug){
                cout<<"eq_Exp_AST, kind 2"<<endl;
            }
            return generate_return();
        }
        else{
            assert(0);
        }
    }

    virtual int get_value() const override {
        if(kind == 0){
            if(debug){
            cout<<"eq exp ast get value,kind 0"<<endl;
            }
            return rel_exp->get_value();
        }
        else if(kind == 1){
            if(debug){
            cout<<"eq exp ast get value,kind 1"<<endl;
            }
            int left = eq_exp->get_value();
            int right = rel_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left == right;
            }
        }
        else if(kind == 2){
            if(debug){
            cout<<"eq exp ast get value,kind 2"<<endl;
            }
            int left = eq_exp->get_value();
            int right = rel_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left != right;
            }
        }
        else{
            if(debug){
                cout<<"EqexpAST get value error"<<endl;
            }
            assert(0);
        }
    }
};

class RelExpAST: public BaseAST {
public:
    int kind;
    // 0 add, 1 < ,2 > ,3 <=, 4 >=
    unique_ptr<BaseAST> rel_exp;
    unique_ptr<BaseAST> add_exp;

    KIR generate_return() const{
        KIR exp_dump_1 = rel_exp->Dump();
        string tmp_cmd_1 = exp_dump_1.s;
        string tmp_var_1 = exp_dump_1.tmp_var;
        KIR exp_dump_2 = add_exp->Dump();
        string tmp_cmd_2 = exp_dump_2.s;
        string tmp_var_2 = exp_dump_2.tmp_var;
        string tmp_var_new = "%" + to_string(tmp_var_cnt++);
        if(debug){
            cout << "tmp_cmd_1: " << tmp_cmd_1 << endl;
            cout << " tmp_var_1: " << tmp_var_1 << endl;
            cout << "tmp_cmd_2: " << tmp_cmd_2 << endl;
            cout << "tmp_var_2: " << tmp_var_2 << endl;
            cout << "tmp_var_new: " << tmp_var_new << endl;
        }
        string op = "";
        if(kind == 1){
            op = " = lt ";
        }
        else if(kind==2){
            op = " = gt ";
        }
        else if(kind==3){
            op = " = le ";
        }
        else if(kind == 4){
            op = " = ge ";
        }
        return KIR(tmp_cmd_1+tmp_cmd_2+" "+tmp_var_new+op+tmp_var_1+", "+tmp_var_2 +"\n",tmp_var_new);
    }
    KIR Dump() const override {
        if(kind==0){
            if(debug){
                cout << "relexpAST, kind 0" <<endl;
            }
            return add_exp->Dump();
        }
        else if(kind==1){
            if(debug){
                cout << "relexpAST, kind 1" <<endl;
            }
            return generate_return();
        }
        else if(kind == 2){
            if(debug){
                cout << "relexpAST, kind 2" <<endl;
            }
            return generate_return();
        }
        else if(kind==3){
            if(debug){
                cout << "relexpAST, kind 3" <<endl;
            }
            return generate_return();
        }
        else if(kind==4){
            if(debug){
                cout << "relexpAST, kind 4" <<endl;
            }
            return generate_return();
        }
        else{
            assert(0);
        }
    }

    virtual int get_value() const override{
        if(kind == 0){
            if(debug){
            cout<<"rel exp ast get value,kind 0"<<endl;
            }
            return add_exp->get_value();
        }
        else if(kind == 1){
            if(debug){
            cout<<"rel exp ast get value,kind 1"<<endl;
            }
            int left = rel_exp->get_value();
            int right = add_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left < right;
            }
        }
        else if(kind == 2){
            if(debug){
            cout<<"rel exp ast get value,kind 2"<<endl;
            }
            int left = rel_exp->get_value();
            int right = add_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left > right;
            }
        }
        else if(kind == 3){
            if(debug){
            cout<<"rel exp ast get value,kind 3"<<endl;
            }
            int left = rel_exp->get_value();
            int right = add_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left <= right;
            }
        }
        else if(kind == 4){
            if(debug){
            cout<<"rel exp ast get value,kind 4"<<endl;
            }
            int left = rel_exp->get_value();
            int right = add_exp->get_value();
            if(left == get_value_err || right == get_value_err){
                return get_value_err;
            }
            else{
                return left >= right;
            }
        }
        else{
            if(debug){
                cout<< "relexpast get value error" <<endl;
            }
            assert(0);
        }
    }
};

class PrimaryExpAST:public BaseAST{
    public:
    int kind;
    unique_ptr<BaseAST> exp;//kind = 0
    unique_ptr<BaseAST> number;//kind = 1
    unique_ptr<BaseAST> lval;//kind = 2

    KIR Dump() const override {
        if(debug){
            cout<<"PrimaryExpAST, kind "<< kind <<endl;
        }
        if(kind == 0){
            return exp->Dump();
        }
        else if(kind == 1){
            return number->Dump();
        }
        else if(kind==2){
            return lval->Dump();
        }
        else{
            assert(0);
        }
    }

    virtual int get_value() const override {
        if(kind == 0){
            if(debug){
            cout<<"PrimaryExpAST get value,kind 0 "<<endl;
            }
            return exp->get_value();
        }
        else if(kind == 1){
            if(debug){
            cout<<"PrimaryExpAST get value,kind 1 "<<endl;
            }
            return number->get_value();
        }
        else if(kind==2){
            if(debug){
            cout<<"PrimaryExpAST get value,kind 2 "<<endl;
            }
            return lval->get_value();
        }
        else{
            if(debug){
                cout<<"PrimaryAST get_value error"<<endl;
            }
            assert(0);
        }
    }
};

class AddExpAST : public BaseAST{
    public:
        unique_ptr<BaseAST> add_exp;
        unique_ptr<BaseAST> mul_exp;
        int kind;//0 mul,1 + ,2 -
        KIR generate_return() const {
            KIR exp_dump_1 = add_exp->Dump();
            string tmp_cmd_1 = exp_dump_1.s;
            string tmp_var_1 = exp_dump_1.tmp_var;
            KIR exp_dump_2 = mul_exp->Dump();
            string tmp_cmd_2 = exp_dump_2.s;
            string tmp_var_2 = exp_dump_2.tmp_var;
            string tmp_var_new = "%" + to_string(tmp_var_cnt++);
            if(debug){
                cout << "tmp_cmd_1 of addexpast: " << tmp_cmd_1 << endl;
                cout << " tmp_var_1 of addexpast: " << tmp_var_1 << endl;
                cout << "tmp_cmd_2 of addexpast: " << tmp_cmd_2 << endl;
                cout << "tmp_var_2 of addexpast: " << tmp_var_2 << endl;
                cout << "tmp_var_new of addexpast: " << tmp_var_new << endl;
            }
            string op = " = add ";
            if(kind==1) op = " = add ";
            else if(kind == 2) op = " = sub ";
            return KIR(tmp_cmd_1 + tmp_cmd_2 + " " + tmp_var_new + op + tmp_var_1 + ", " + tmp_var_2 + "\n",tmp_var_new);
        }
        KIR Dump() const override{
            if(debug){
                cout<<"addexpAST, kind "<< kind <<endl;
            }
            if(kind == 0){
                return mul_exp->Dump();
            }
            else if(kind==1){
                return generate_return();
            }
            else if(kind == 2){
                return generate_return();
            }
            else{
                assert(false);
            }
        }
        virtual int get_value() const override {
            if(kind == 0){
                if(debug){
                cout<<"addAST get value,kind 0 "<<endl;
                }
                return mul_exp->get_value();
            }
            else if( kind == 1){
                if(debug){
                cout<<"addAST get value,kind 0 "<<endl;
                }
                int left = add_exp->get_value();
                int right = mul_exp->get_value();
                if(left == get_value_err || right == get_value_err){
                    return get_value_err;
                }
                else{
                    return left + right;
                }
            }
            else if(kind == 2){
                if(debug){
                cout<<"addAST get value,kind 0 "<<endl;
                }
                int left = add_exp->get_value();
                int right = mul_exp->get_value();
                if(left == get_value_err || right == get_value_err){
                    return get_value_err;
                }
                else{
                    return left - right;
                }
            }
            else{
                if(debug){
                    cout<<"AddExpAST get value error"<<endl;
                }
                assert(0);
            }
        }
};

class MulExpAST : public BaseAST{
    public:
        int kind;
        //0 unary, 1 * ,2 / ,3 %
        unique_ptr<BaseAST> mul_exp;
        unique_ptr<BaseAST> unary_exp;
        KIR generate_return()const{
            KIR exp_dump_1 = mul_exp->Dump();
            string tmp_cmd_1 = exp_dump_1.s;
            string tmp_var_1 = exp_dump_1.tmp_var;
            KIR exp_dump_2 = unary_exp->Dump();
            string tmp_cmd_2 = exp_dump_2.s;
            string tmp_var_2 = exp_dump_2.tmp_var;
            string tmp_var_new = "%" + to_string(tmp_var_cnt++);
            if(debug){
                cout << "tmp_cmd_1: " << tmp_cmd_1 << endl;
                cout << " tmp_var_1: " << tmp_var_1 << endl;
                cout << "tmp_cmd_2: " << tmp_cmd_2 << endl;
                cout << "tmp_var_2: " << tmp_var_2 << endl;
                cout << "tmp_var_new: " << tmp_var_new << endl;
            }
            string op = " = mul ";
            if(kind==1) op = " = mul ";
            else if(kind == 2) op = " = div ";
            else if(kind==3) op = " = mod ";
            return KIR(tmp_cmd_1 + tmp_cmd_2 + " " + tmp_var_new + op + tmp_var_1 + ", " + tmp_var_2 + "\n",tmp_var_new);
        }
        KIR Dump()const override{
            if(debug){
                cout<<"mulexp,kind: "<<kind<<endl;
            }
            if(kind==0){
                return unary_exp->Dump();
            }
            else if(kind==1){
                return generate_return();
            }
            else if(kind==2){
                return generate_return();
            }
            else if(kind==3){
                return generate_return();
            }
            else{
                assert(0);
            }
        }
        virtual int get_value() const override {
            if(kind == 0){
                if(debug){
                cout<<"MulAST get value,kind 0 "<<endl;
                }
                return unary_exp->get_value();
            }
            else if(kind == 1){
                if(debug){
                cout<<"MulAST get value,kind 1 "<<endl;
                }
                int left = mul_exp->get_value();
                int right = unary_exp->get_value();
                if(left == get_value_err || right == get_value_err){
                    return get_value_err;
                }
                else{
                    return left * right;
                }
            }
            else if(kind == 2){
                if(debug){
                cout<<"MulAST get value,kind 2 "<<endl;
                }
                int left = mul_exp->get_value();
                int right = unary_exp->get_value();
                if(left == get_value_err || right == get_value_err || right == 0){
                    return get_value_err;
                }
                else{
                    return left / right;
                }
            }
            else if(kind == 3){
                if(debug){
                cout<<"MulAST get value,kind 3 "<<endl;
                }
                int left = mul_exp->get_value();
                int right = unary_exp->get_value();
                if(left == get_value_err || right == get_value_err || right == 0){
                    return get_value_err;
                }
                else{
                    return left % right;
                }
            }
            else{
                if(debug){
                    cout << " mulexp get_value error"<<endl;
                }
                assert(0);
            }
        }
};

class UnaryExpAST : public BaseAST{
    public:
        int kind;
        //0 prime
        //1 op exp
        unique_ptr<BaseAST> primary_exp;
        unique_ptr<BaseAST> unary_exp;
        unique_ptr<BaseAST> unary_op;
        string func_name;
        vector<unique_ptr<BaseAST>> args;
        KIR Dump()const override{
            if(kind==0){
                if(debug){
                    cout<<"UnaryAST,kind 0"<<endl;
                }
                return primary_exp->Dump();
            }
            else if(kind==1){
                if(debug){
                    cout<<"UnaryAST,kind 1"<<endl;
                }
                string op = unary_op->Dump().tmp_var;
                KIR exp_dump = unary_exp->Dump();
                string tmp_cmd = exp_dump.s;
                string tmp_var = exp_dump.tmp_var;
                string tmp_var_new = "%" + to_string(tmp_var_cnt++);
                if(debug){
                    cout<<"op: "<< op <<endl;
                    cout<<"tmp_cmd: "<<tmp_cmd<<endl;
                    cout<<"tmp_var: "<<tmp_var<<endl;
                    cout<<"tmp_var_new: "<<tmp_var_new<<endl;
                }
                if(op == "!"){
                    return KIR(tmp_cmd + " " + tmp_var_new + " = eq " + tmp_var +", 0 \n",tmp_var_new);
                }
                else if(op== "-"){
                    return KIR(tmp_cmd + " " + tmp_var_new + " = sub 0, " + tmp_var + "\n",tmp_var_new);
                }
                else if(op == "+"){
                    return exp_dump;
                }
                else{
                    cout<<"unknown op"<<endl;
                    assert(0);
                }
            }
            else if(kind == 2){
                if (debug) {
                    cout << "UnaryExpAST Dumping Case 2" << endl;
                    cout << "func_name: " << func_name << endl;
                }

                string args_ir = "";  //计算出每个参数所需要的IR
                string args_var = ""; //所有参数的字符串形式
                int first = 1;        //判断是否是第一个参数
                for (auto &arg : args) {
                    auto arg_dump = arg->Dump();
                    args_ir += arg_dump.s;
                    args_var += (first == 1 ? "" : ", ") + arg_dump.tmp_var;
                    first = 0;
                }
                string call_func_str = "call @" + func_name + "(" + args_var + ")";

                int has_ret = symbol_table->find(func_name)->kind == 3; //判断是否有返回值
                if (has_ret) {
                    string tmpVarNew = "%" + to_string(tmp_var_cnt++);
                    string ret_code = args_ir + "  " + tmpVarNew + " = " + call_func_str + "\n";
                    return KIR(ret_code, tmpVarNew);
                } else {
                    string ret_code = args_ir + "  " + call_func_str + "\n";
                    return KIR(ret_code, "");
                }
            }
            else{
                assert(0);
            }
        }

        virtual int get_value() const override {
            if(kind == 0){
                if(debug){
                cout<<"unaryexpAST get value,kind 0 "<<endl;
                }
                return primary_exp->get_value();
            }
            else if(kind == 1){
                if(debug){
                cout<<"unaryexpAST get value,kind 1 "<<endl;
                }
                string op = unary_op->Dump().tmp_var;
                int ret = unary_exp->get_value();
                if(ret == get_value_err){
                    return get_value_err;
                }
                if(op == "!"){
                    return !ret;
                }
                else if(op == "-"){
                    return -ret;
                }
                else if(op == "+"){
                    return ret;
                }
                else{
                    if(debug){
                        cout<<"UnaryExpAST get value error, op error"<<endl;
                    }
                    assert(0);
                }
            }
            else if(kind == 2){
                return get_value_err;
            }
            else{
                if(debug){
                    cout<<"UnaryexpAST get value error" <<endl;
                }
                assert(0);
            }
        }
};

class UnaryOpAST: public BaseAST{
    public:
    int kind;//0 +, 1 -,2 !
    string op;
    KIR Dump()const override{
        return KIR("",op);
    }
};

class NumberAST : public BaseAST {
public:
    // -- INT_CONST
    int int_const = 0;
    NumberAST() = default;
    NumberAST(int _int_const) : int_const(_int_const) {}

    KIR Dump() const override {
        string s = to_string(int_const);
        return KIR("",s);
    }

    virtual int get_value()const override{
        if(debug){
            cout<<"numberAST get value "<<endl;
        }
        return int_const;
    }
};

class DeclAST : public BaseAST{
    public:
        int kind;//0 const,1 var
        unique_ptr<BaseAST> const_decl;
        unique_ptr<BaseAST> var_decl;
        KIR Dump() const override{
            if(kind == 0){
                return const_decl->Dump();
            }
            else{
                return var_decl->Dump();
            }
        }
};

static vector<int> *init_array(const vector<int>*array_types,int k,const vector<unique_ptr<BaseAST> > *init_values){
    vector<int> *ret = new vector<int>();
    int pos = 0;
    int s = 1;
    for(int j = 0;j<=k;++j){
        s*=(*array_types)[j];
    }
    if(k == 0){
        for(auto&p:*init_values){
            int val = p->get_value();
            ret->push_back(val);
            pos++;
        }
    }
    else{
        for(auto&p: *init_values){
            InitValAST *init_val = (InitValAST*)p.get();
            int kind = init_val->kind;
            if(kind == 0){
                int val = atoi(p->Dump().tmp_var.c_str());
                ret->push_back(val);
                pos++;
            }
            else{
                if(pos % (*array_types)[0] != 0){
                    int comp = (*array_types)[0] - (pos % (*array_types)[0]);
                    pos += comp;
                    for(int j = 0; j< comp;++j){
                        ret->push_back(0);//补齐0
                    }
                }
                //继续递归调用
                int align = 0;
                int tmp_pos = pos;
                for(align = 0;align < k;++align){
                    if(tmp_pos % (*array_types)[align] == 0){
                        tmp_pos /= (*array_types)[align];
                    }
                    else break;
                }
                auto arr_ret = init_array(array_types,align - 1, &init_val->init_vals);
                for(int i : (*arr_ret)){
                    ret->push_back(i);
                }
                pos += (*arr_ret).size();
            }
        }
    }
    if(ret->size() < s){
        for(int i=ret->size();i < s;++i){
            ret->push_back(0);
        }
    }
    return ret;
}

// ...

