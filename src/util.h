#pragma once
#include<iostream>
#include<cstdio>
#include<cstring>
#include<vector>
#include<map>
#include"koopa.h"
#include<limits.h>

using namespace std;

static int debug = 0;//调试模式
static int tmp_var_cnt = 0;//临时变量标号
static int basic_block_cnt = 0;//基本块数量
static int block_cnt = 0;//block数量
static int get_value_err = INT_MAX - 2048;

//符号表条目
class SymbolTableEntry{
public:
  int kind;//0-const,1-var,2 无返回值，3有返回值，4数组，5指针
  int constant;
  string addr;
  vector<int>* array_types;

  SymbolTableEntry(){};

  SymbolTableEntry(int k, int c){
    if(debug){
      cout<<"build entry as const"<<endl;
      cout<< k << " "<< c <<endl;
    }
    if(k != 0){
      if(debug){
        cout<< " kind error,not const var"<<endl;
      }
      assert(0);
    }
    this->kind = k;
    this->constant = c;
    this->addr = "";
  }
  SymbolTableEntry(int k, string a){
    if(debug){
      cout<<"build entry as var"<<endl;
      cout<<"k: "<<k << " a: "<<a<<endl;
    }
    
    if(k != 1){
      if(debug){
        cout<< " kind error,not var"<<endl;
      }
      assert(0);
    }
    this->kind = k;
    this->addr = a;
    this->constant = 0;
  }
  SymbolTableEntry(int k){
    if(debug){
      cout<<"build entry only kind"<<endl;
    }
    this->kind = k;
    this->constant = 0;
    this->addr = "";
  }
  SymbolTableEntry(int k,vector<int>* at,string a){
    if(debug){
      cout<<"build entry as array"<<endl;
    }
    this->kind = k;
    this->constant = 0;
    this->addr = a;
    this->array_types = at;
  }
};
//符号表
class SymbolTable{
  public:
  map<string,SymbolTableEntry> table;
  SymbolTable* parent = nullptr;
  int num = 0;//编号
  int cnt = 0;//变量数
  SymbolTable(){};
  SymbolTable(SymbolTable *parent){
    this->parent = parent;
    block_cnt++;
    this->num = block_cnt;
  }
  void Insert(string name, SymbolTableEntry entry){
    ++cnt;
    table[name] = entry;
    if(debug)
      cout<<"name :" <<name<<" kind: "<<entry.kind<<endl;
  }
  SymbolTableEntry* find(string name){
    if(table.find(name) != table.end()){
      return &table[name];
    }
    else if(parent != nullptr){
      return parent->find(name);
    }
    else{
      if(debug){
        cout << "can't find symbol error,cnt:" << cnt<<endl;
      }
      return nullptr;
    }
  }
};

class KIR{
    public:
    string s = "";
    string type = "i32";
    string tmp_var = "";
    vector<int> *array_val;
    int end = 0;

    KIR(){}
    KIR(string _s){this->s = _s;}
    KIR(string s,string var){
      this->s = s;
      this->tmp_var = var;
    }
    KIR(string s, int _end){
      this->s = s;
      this->end = _end;
    }
};

class R_code{
public:
  string code = "";
  int retAddr = INT_MAX - 2048; //error 值
  int append = 0;
  R_code(){}
  R_code(string _s){
    this->code = _s;
  }
  R_code(string s, int addr){
    this->code = s;
    this->retAddr = addr;
  }
  R_code(string s,int addr,int a){
    this->code = s;
    this->retAddr = addr;
    this->append = a;
  }
};

class R_Info{
  public:
    string func_name = "";
    int stack_bio = 0;
    int stack_size = 0;
    string reg = "t0";
    koopa_raw_slice_t func_param;
    koopa_raw_slice_t bb_param;
    R_Info(){};
};

// 所有 AST 的基类
class BaseAST {
 public:
  virtual ~BaseAST() = default;
  virtual KIR Dump() const = 0;
  virtual KIR Dump(const vector<int> *array_types) const {
    return Dump();
  }
  virtual KIR get_var_addr(){
    return KIR("","error_addr");
  }
  virtual int get_value() const{
    if(debug){
      cout<< " get value error "<<endl;
    }
    return INT_MAX - 2048;
  };

};

class LoopInfo {
  public:
    string start_label = "";
    string end_label = "";
    LoopInfo* parent;
    LoopInfo(string _s,string _e, LoopInfo* _p){
      this->start_label = _s;
      this->end_label = _e;
      this->parent = _p;
    } 
};

static string new_label(){
  return "%Label_" + to_string(basic_block_cnt++);
}

static string get_type_name(const vector<unique_ptr<BaseAST>> *array_types){
  string ret = "i32";
  for(auto & i: *array_types){
    int size = i->get_value();
    ret = "[" + ret + ", " + to_string(size) + "]";
  }
  return ret;
}
//存储数组
static string store_array(string array_name,const vector<int> *array_types,const vector<int> *array_values){
  int k = (*array_types).size();//数组维数
  string s = "";
  int l  = (*array_values).size();
  for(int i = 0;i < l;++i){
    int tmp_i = i;
    vector<int> indexs;
    for(int j = 0;j < k;++j){
      indexs.push_back(tmp_i % (*array_types)[j]);
      tmp_i /= (*array_types)[j];
    }
    string last_name = array_name;
    for(int j = k - 1 ;j >= 0;--j){
      string this_name = "%" + to_string(tmp_var_cnt++);
      s.append("  " + this_name + " = getelemptr " + last_name + ", " + to_string(indexs[j]) + "\n");
      last_name = this_name;
    }
    s.append("  store " + to_string((*array_values)[i]) + ", " + last_name + "\n");
  }
  return s;
}

static string int_to_string(const vector<int>*array_types,const vector<int>*array_values,int k){
  string s = "{ ";
  if(k==0){
    int l = (*array_values).size();
    for(int i =0 ; i<l;++i){
      if(i > 0){
        s += ", ";
      }
      s += to_string(array_values->at(i));
    }
    s += " }";
  }
  else{
    int tmp = 1;
    for(int j = 0;j<k;++j){
      tmp*=(*array_types)[j];
    }//之前所有维数的大小加起来
    for(int j=0;j<(*array_types)[k];++j){
      if(j > 0) s += ", ";
      vector<int> tmp_array;
      for(int i = j * tmp;i < (j+1) * tmp;++i){
        tmp_array.push_back((*array_values)[i]);
      }
      s += int_to_string(array_types,&tmp_array,k - 1);//对第k-1维继续生成
    }
    s += " }";
  }
  return s;
}
