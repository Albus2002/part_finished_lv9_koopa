SysT_RISC
实验文档
一.编译器概述
1.基本功能
1.将Sysy语言编译为Koopa-IR形式输出，文件后缀为.koopa,编译命令为：
build/compiler -koopa hello.c -o hello.koopa
2.将Sysy语言编译为RISCV形式输出，编译命令为：

build/compiler -riscv hello.c -o hello.v
2.编译器功能
开发的编译器特点是：
1.代码冗长
2.容易出现各种奇怪的bug
3.能勉强完成testcase.
项目包含了作业要求中的基本功能，能够实现对简单的c程序编译并输出Koopa IR 和 RISCV语言形式的文件，目前支持的指令有：
return指令，逻辑运算（||和&&），基本代数运算（+，-，*，/），if else 分支判断（无二义性歧义），while循环和continue、break关键字，函数调用和传参，int和void类函数实现。
二.编译器架构设计
1.主要模块组成
整体编译器依照MaxXing给出的实验文档进行搭建，所有源文件都位于main/src文件夹中，根据Makefile进行编译。编译时使用的指令为：
mkdir build
cd build
make
此时在build文件夹中会产生编译器的所有可执行文件。后续使用时，利用概述中提到的命令，并将文件名替换为对应的c语言文件即可。
src文件夹中包含6个文件，分别是ast.h,kpa2V.h,main.cpp,sysy.l,sysy.y和util.h。下面分别介绍六个文件的内容和对应功能：
ast.h
此文件包含了全部抽象语法树的对应类，包括基础语法树和实验文档中提到的全部扩展语法树，以及一些用于中途生成必要字符串的函数。
kpa2V.h
此文件包含了全部将Koopa-IR转换为RISCV的代码。具体执行方式为：依照koopa.h中的数据结构对其进行遍历，然后根据每一块数据结构的关系进行最终的输出。
main.cpp
是项目的入口，编译器通过此文件执行来编译c语言文件。
sysy.y
此文件包含了所有抽象语法树的语法分析，是语法分析器的主要部分。此文件中定义了所有语义结构和语法规则。
sysy.l
此文件是词法分析器的主要部分，里面定义了所有符号和关键字。
util.h
是项目类的补充部分，包含了kpa2V和ast.h中需要的一些数据结构，函数和全局静态变量，以及调试模式必要的参数。
2主要数据结构
抽象语法树（AST）及其衍生类
抽象语法树的类型很多，因为在实现编译器的时候采用的思路是每一个AST都会对应一个class，因此种类大概有20多个，在这里只阐述几个比较典型的语法树结构：
BaseAST
所有抽象语法树的基础类型，属于virtual类。其中包含了:
  语法树的输出函数Dump()，用于输出对应的Koopa IR。
  语法树的值获取函数get_value()（用于必要的优化步骤，对于表达式可以在编译过程中直接计算出值）。
  语法树的目的地址（程序中表现为名称）获取函数get_var_addr()。
class BaseAST {
 public:
  virtual ~BaseAST() = default;
  virtual KIR Dump() const = 0;
  virtual KIR Dump(const vector<int> *array_types) const {
    return Dump();
  }
  virtual KIR get_var_addr(){
    return KIR("","error_addr");
  }
  virtual int get_value() const{
    if(debug){
      cout<< " get value error "<<endl;
    }
    return INT_MAX - 2048;
  };
};
衍生AST
这里以一个比较复杂的AST-MatchedStmtAST为例，其余AST的实现思路和它基本一致。
AST的基础属性中的unique_ptr<BaseAST>是语法规则后续对应的所有AST,对于MatchedStmtAST来说，后续可能被翻译成的语法树结构有exp,lval,block和matchedstmt，这里的1和2下标是用于区分if else分支的。对于MatchedStmtAST,所有的基础属性会在语义分析的时候被初始化，再通过调用Dump()函数生成对应的koopa ir。
基础属性中的kind标识用于区分MatchedStmtAST走了哪一条翻译路径。以case 0 为例，kind == 0表示这个matchedstmtAST的exp指针不为空，而且只被翻译成了Exp这一语法结构。
最终，所有的代码会在KIR数据结构的string s属性中储存，并以KIR数据结构形式返回到初始的AST中去。main.cpp程序会调用初始AST对应的dump()函数，并将s输出。
class MatchedStmtAST:public BaseAST{
    public:
    unique_ptr<BaseAST> exp;
    unique_ptr<BaseAST> lval;
    unique_ptr<BaseAST> block;
    unique_ptr<BaseAST> matched_stmt_1;
    unique_ptr<BaseAST> matched_stmt_2;
    int kind;
    //kind = 0 is exp
    KIR Dump() const override{
        if(kind == 0){
            if(debug){
                cout<<"Matchedstmt, kind 0, exp"<<endl;
            }
            auto exp_dump = exp->Dump();
            string tmp_s = exp_dump.s;
            string tmp_var = exp_dump.tmp_var;
            return KIR(tmp_s + " ret " + tmp_var + "\n",1);//end of control stream
        }
        else if(kind == 1){
            if(debug){
                cout<<"MatchedStmtAST,kind 1, lval"<<endl;
            }
            KIR exp_dump = exp->Dump();
            string tmp_ir = exp_dump.s;
            string tmp_var = exp_dump.tmp_var;

            KIR lval_dump = lval->get_var_addr();
            tmp_ir.append(lval_dump.s);
            return KIR(tmp_ir + "  store " + tmp_var + ", " + lval_dump.tmp_var + "\n");
        }
        else if(kind == 2){
            if(debug){
                cout<<"MatchedStmtAST, kind 2,block "<<endl;
            }
            return block->Dump();
        }
        else if(kind == 3){
            if(debug){
                cout<<"MatchedStmtAST, kind 3,Exp "<<endl;
            }
            return exp->Dump();
        }
        else if(kind == 4){
            if(debug){
                cout<<"MatchedStmtAST, kind 4,empty "<<endl;
            }
            return KIR("");
        }
        else if(kind == 5){
            if(debug){
                cout<<"MatchedStmtAST, kind 5,if "<<endl;
            }
            KIR exp_dump = exp->Dump();
            string tmp_ir = exp_dump.s;
            string tmp_var = exp_dump.tmp_var;
            string true_label = new_label();
            string false_label = new_label();
            string end_label = new_label();
            
            KIR ret_1 = matched_stmt_1->Dump();
            KIR ret_2 = matched_stmt_2->Dump();

            string s = tmp_ir + "  br " + tmp_var + ", " + true_label 
                     + ", " + false_label + true_label + ":\n" + ret_1.s;
            if(ret_1.end == 0){
                s += "  jump " + end_label + "\n";
            }
            s += false_label + ":\n" + ret_2.s;
            if(ret_2.end == 0){
                s += "  jump " + end_label + "\n";
            }
            s += end_label + ":\n";
            return KIR(s);
        }
        else if(kind == 6){
            if(debug){
                cout<<"MatchedStmtAST, kind 6,break "<<endl;
            }
            if(loop_info == nullptr){
                if(debug){
                    cout<<"break without while"<<endl;
                }
                assert(0);
            }
            else{
                string s = "  jump " + loop_info->end_label + "\n";
                return KIR(s,1);
            }
        }
        else if(kind == 7){
            if(debug){
                cout<<"MatchedStmtAST, kind 7,continue "<<endl;
            }
            if(loop_info == nullptr){
                if(debug){
                    cout<<"continue without while"<<endl;
                }
                assert(0);
            }
            else{
                string s = "  jump " + loop_info->start_label + "\n";
                return KIR(s,1);
            }
        }
        else if(kind == 8){
            if(debug){
                cout<<"MatchedStmtAST, kind 8,while "<<endl;
            }
            KIR exp_dump = exp->Dump();
            string tmp_ir = exp_dump.s;
            string tmp_var =exp_dump.tmp_var;
            string while_start = new_label();
            string while_body = new_label();
            string end_label = new_label();

            LoopInfo* tmp_loop = new LoopInfo(while_start,end_label,loop_info);
            loop_info = tmp_loop;

            KIR matched_stmt_dump = matched_stmt_1->Dump();
            //循环起始
            string s = "  jump " + while_start + "\n";
            s += while_start + ":\n" + tmp_ir + "  br " + tmp_var + ", " + while_body + ", " + end_label + "\n";

            //循环体
            s += while_body + ":\n" + matched_stmt_dump.s;
            if(matched_stmt_dump.end == 0){
                s += "  jump " + while_start + "\n";
            }

            s += end_label + ":\n";

            loop_info = loop_info->parent;
            
            return KIR(s);
        }
        else{
            if(debug){
                cout<<"MatchedStmtAST, kind unknown!"<<endl;
            }
            assert(0);
        }
    }
};

KIR
是语法树结构的Dump()返回值类型。具体的代码如下：
class KIR{
    public:
    string s = "";
    string type = "i32";
    string tmp_var = "";
    int end = 0;

    KIR(){}
    KIR(string _s){this->s = _s;}
    KIR(string s,string var){
      this->s = s;
      this->tmp_var = var;
    }
    KIR(string s, int _end){
      this->s = s;
      this->end = _end;
    }
};
其中，s属性代表最终要输出的Koopa IR代码，type代表当前返回的KIR的函数类型，tmp_var代表当前返回的变量名称（例如，在函数调用中计算出了一个值，我们要存储它对应的%+编号作为名称），end变量表示当前KIR是否是一段代码的结束，用于while循环的判定。
R_Info
R_Info是后端用于解析Koopa IR转换的koopa_raw_value_t所设计的数据结构。具体代码如下：
class R_Info{
  public:
    string func_name = "";
    int stack_bio = 0;
    int stack_size = 0;
    string reg = "t0";
    koopa_raw_slice_t func_param;
    koopa_raw_slice_t bb_param;
    R_Info(){};
};
func_name代表了当前info对应的函数名称；
stack_bio代表了当前info的栈偏移值；
stack_size则代表了当前的栈大小；
对于寄存器，默认返回值寄存器是"t0"；
后续的func_param和bb_param则是用于在block和函数体内进行参数传递。
R_code
R_code是后端最终用于输出代码的数据结构。具体代码如下：
class R_code{
public:
  string code = "";
  int retAddr = INT_MAX - 2048; //error 值
  int append = 0;
  R_code(){}
  R_code(string _s){
    this->code = _s;
  }
  R_code(string s, int addr){
    this->code = s;
    this->retAddr = addr;
  }
  R_code(string s,int addr,int a){
    this->code = s;
    this->retAddr = addr;
    this->append = a;
  }
};
其中，code是最终要输出的riscv代码；
retAddr是函数的返回地址（此处展示的是初始化值，如果产生函数调用，会对其赋值为正确的返回地址）；
append表示此代码是否需要被插入到对应的riscv代码中，如果为0表示无需插入。
符号表与符号表条目
符号表的代码如下所示：
class SymbolTable{
  public:
  map<string,SymbolTableEntry> table;
  SymbolTable* parent = nullptr;
  int num = 0;//编号
  int cnt = 0;//变量数
  SymbolTable(){};
  SymbolTable(SymbolTable *parent){
    this->parent = parent;
    block_cnt++;
    this->num = block_cnt;
  }
  void Insert(string name, SymbolTableEntry entry){
    ++cnt;
    table[name] = entry;
    if(debug)
      cout<<"name :" <<name<<" kind: "<<entry.kind<<endl;
  }
  SymbolTableEntry* find(string name){
    if(table.find(name) != table.end()){
      return &table[name];
    }
    else if(parent != nullptr){
      return parent->find(name);
    }
    else{
      if(debug){
        cout << "can't find symbol error,cnt:" << cnt<<endl;
      }
      return nullptr;
    }
  }
};
其中，table是符号表的存储结构，是存储<string ,entry>的map；
parent是指向上一级符号表的指针；
num是当前符号表的编号；
cnt是当前符号表内存储的条目数量/变量数量。

符号表条目的代码如下所示：
class SymbolTableEntry{
public:
  int kind;//0-const,1-var
  int constant;
  string addr;
  vector<int>* array_types;

  SymbolTableEntry(){};

  SymbolTableEntry(int k, int c){
    if(debug){
      cout<<"build entry as const"<<endl;
      cout<< k << " "<< c <<endl;
    }
    if(k != 0){
      if(debug){
        cout<< " kind error,not const var"<<endl;
      }
      assert(0);
    }
    this->kind = k;
    this->constant = c;
    this->addr = "";
  }
  SymbolTableEntry(int k, string a){
    if(debug){
      cout<<"build entry as var"<<endl;
      cout<<"k: "<<k << " a: "<<a<<endl;
    }
    
    if(k != 1){
      if(debug){
        cout<< " kind error,not var"<<endl;
      }
      assert(0);
    }
    this->kind = k;
    this->addr = a;
    this->constant = 0;
  }
  SymbolTableEntry(int k){
    if(debug){
      cout<<"build entry only kind"<<endl;
    }
    this->kind = k;
    this->constant = 0;
    this->addr = "";
  }
  SymbolTableEntry(int k,vector<int>* at,string a){
    if(debug){
      cout<<"build entry as array"<<endl;
    }
    this->kind = k;
    this->constant = 0;
    this->addr = a;
    this->array_types = at;
  }
};
其中，kind是符号表条目对应变量的种类：
0为const变量；
1为普通变量；
2为无返回值的函数；
3为有返回值的函数；
4为指针；
5为数组。
addr属性对应的是变量名称（名称对应的就是地址入口）。
constant对应的是变量值（如果有的话）。
array_type表示的是数组的类型。
函数传参
此处设计的函数传参是通过FuncFParam的语法树结构进行实现的。在FuncFParam的结构中，Dump()函数会将对应的函数参数存到给定变量名称中，并创建符号表条目用于记录变量名称和对应数值。这样在后续的函数调用中，函数只需要到符号表查找对应名称和数值就可以生成KoopaIR的代码了。
3 主要设计考虑及算法选择
3.1 符号表的设计考虑
符号表在设计时没有考虑很多东西，最开始只有kind属性，变量的值和变量名称。因为sysy.y文件会解析所有对应的作用域，所以实际上符号表是不需要考虑语法分析里面的工作的——只需要保证在对应作用域查找的变量一定是当前作用域内的就可以。
3.2 寄存器分配策略
寄存器分配中，所有的“a”开头的寄存器用于存储参数，“t”开头的寄存器用于计算中间值和返回值。在此处，函数传参时遵循了惯有规则，超过八个参数时后续参数会通过栈而不是寄存器传递。
3.3 采用的优化策略
目前主要的优化策略是用get_value函数提前计算出表达式的值（前提是表达式是正确的），这样在生成koopa IR的时候可以省略很多步骤，比如return后面跟了一个很大的计算式，最终可能只会生成ret 1或者ret 0.这样，后续生成的riscv会减少非常多不必要的操作。同时，函数传参时遵循了惯有规则，超过八个参数时后续参数会通过栈而不是寄存器传递。其他方面则没有进行什么优化。
三.编译器实现细节
3.1 各阶段编码细节
Lv1. main函数和Lv2. 初试目标代码生成
这部分的主要代码都比较简单，只需要跟着实验文档的指示书写即可。但是要注意设定好数据结构之后，函数必须要返回对应数据结构，不然可能会出现奇怪的bug：比如A函数里面的B函数没有写返回值，那么B函数里面的C函数很有可能会被莫名其妙地执行两次而不会报错（当时这个bug查了巨久都没有查出来为什么，一直以为是分支条件写错了）
Lv3. 表达式
比较简单的一个level，但是要写的数据结构是真的多。大坨大坨的xxxAST代码看着一样又不太一样，Add和Mul的优先级问题，逻辑符号和算术表达式的优先级问题，还有生成字符串的时候寄存器的名称还有中间的空格数量等等都被卡了很久，每次都是csdn上现场补习ics知识；这里为了图省事，按照文档中的提示每一个AST都写了对应的class类，好处是调用的时候非常清晰，坏处就是代码太长了...严重不符合简洁优美的要求，但是自己也没有什么代码重构的天赋，所以就这样吧。
Lv4. 常量和变量
还算友好的一个level，写的时候重点在于多的一些AST要如何设计。同时，为了给level9做一些铺垫，我试了试能不能直接把数组模块也加进去，后果就是程序反复的崩溃（尽管有时候我都不知道为什么会进入奇怪的分支），最后只能删了。
以及，符号表的初始化也遇到了好几个奇怪的bug，其中之一就是初始化函数的参数不能和对应属性重名（比如kind），我印象里老师上课说过只要加了this->关键字就不会有错，但是实际运行起来就是tnnd会出毛病，kind会被初始化成一个好几百万的大数。最后，调半天调不明白，改了变量名称了事。
Lv5. 语句块和作用域
这部分的设计主要参考了类似链表的结构，因为block总是一个一个一个出现的，具体写的时候没有什么很离谱的地方。最难绷的是两个testcase（我承认确实写的时候没想到），一个出现了1+2;这种老师上课讲c++就表示不能这么写的形式，还有一个居然只有一个分号，从docker里抓出来testcase的时候差点昏过去，调了半个小时死活没输出的原因居然是内容是空白的。很抽象，但是testcase确实考虑的非常全面。
Lv6. if语句
在这里采取解决二义性的方法就是新引入一个AST:otherstmtAST，意为将这个ast和原本的matchedast区分开来，专门用来匹配单独一行的if，使得后续的语法树构建时可以做到if和else的最近优先匹配。
Lv7. while语句
while循环这里的处理方式和if差不多，也是通过两个不同的Stmt分别对二义性语句进行区分：matched代表已经一一匹配的包含while的if else，other代表第一个单独一行的while里面的if。
Lv8. 函数和全局变量
lv8的难度和复杂程度比起前三个level直接提升了一档，这块调试了将近三天才差不多调通...最开始因为只考虑了初始函数的参数，没有考虑main函数体内的函数参数，写完了之后发现彻底疯狂，一个测试点都过不去；然后只考虑了函数内参数，没有考虑全局定义导致反复报错；同时因为想要直接复用Functype的AST，导致全局变量的处理死活过不去语法分析，后来发现是产生了规约/规约冲突：functype的INT分支和btype都可以代表INT，为此只能把functype直接拆开来写了，但是此时的项目已经成为了史山，因为莫名其妙的原因改了之后直接报错找不到“#include <iostream>”...真难绷，只能重新拉了一份代码重写。
写后端的时候最大的问题就是参数放在哪。最开始的想法是直接全扔寄存器上，但是看了看后面感觉可能这么写过不去，于是改成超过八个参数就放在栈上。但是栈偏移值总是出错...后来发现自己把偏移值和栈大小赋反了，改正之后就没有问题了。
Lv9. 数组
对于这个level，如果不让我说“ctmd”，那我真的没有什么好说的了。

3.2 工具软件介绍（若未使用特殊软件或库，则本部分可略过）
1. 例如flex/bison：（100字以内，说明你利用这个工具/模块/库做了什么）
2. 例如libkoopa：（同上）
3. 其它软件或库：（同上）

四、实习总结

4.1 收获和体会
谢谢编译原理lab增强我调试代码的能力。
4.2 学习过程中的难点，以及对实习过程和内容的建议
实验文档写的真的很好！如果这门课能给80分，至少60分得是这个文档的功劳。
只可惜上编译课的学期MaxXing已经不担任编译原理的课程助教了。
4.3 对老师讲解内容与方式的建议
老师们上课讲的很好，但是考试题出得简单一些就更好了。
